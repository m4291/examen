from pathlib import Path
# import .. utils as ut
from .. import utils
# import temppathlib


def test_check_delete_file() -> None:

   # create a new file
    path = Path.cwd().joinpath(Path("src/tests/tmp"))
    filename = Path("testfile.txt")
    filePath = path.joinpath(filename)
    print(filePath)
    filePath.touch(exist_ok=True)
    file = open(filePath, "w")

    utils.cleanfiles(path, filename)

    assert Path(filePath).is_file() == False


def test_check_wrong_file() -> None:
    # create a new file
    path = Path.cwd().joinpath(Path("src/tests/tmp"))
    filename = Path("testfile.txt")
    filePath = path.joinpath(filename)
    filePath.touch(exist_ok=True)
    file = open(filePath, "w")
    assert filename not in utils.getfilesnames(filePath)


def test_check_good_file() -> None:
    path = Path.cwd().joinpath(Path("data/raw"))
    filename = Path("cobra.txt")
    filePath = path.joinpath(filename)
    filePath.touch(exist_ok=True)
    file = open(filePath, "w")
    assert str(filePath) in utils.getfilesnames(filePath)
    utils.cleanfiles(path, filename)


def test_check_read_csv_file() -> None:
    path = Path.cwd().joinpath(Path("src/tests/testdata/raw"))
    filename = Path("testatlcrime.csv")
    filePath = path.joinpath(filename)
    df = utils.validatedataframe(filePath, 3)
    assert not df.empty


def test_data_columns() -> None:
    path = Path.cwd().joinpath(Path("src/tests/testdata/raw"))
    filename = Path("testatlcrime.csv")
    filePath = path.joinpath(filename)
    assert utils.getfilecolums(filePath, 3) != []


# def test_clean_columns() -> None:
#     path = Path.cwd().joinpath(Path("src/tests/testdata/raw"))
#     filename = Path("testCOBRA-2020.csv")
#     filePath = path.joinpath(filename)
#     realcolumns = ['offense_id', 'rpt_date', 'occur_date', 'occur_time', 'poss_date',
#                    'poss_time', 'beat', 'location', 'UC2_Literal', 'neighborhood', 'npu', 'lat', 'long']
#     df = utils.cleancolums(filePath, 3)
#     assert df.columns == realcolumns

# def test_data_columns_match() -> None:
#     path = Path.cwd().joinpath(Path("src/tests/testdata/raw"))
#     filename = Path("testCOBRA-2020.csv")
#     filePath = path.joinpath(filename)
#     assert utils.checkmatchcols(filePath, 3) == True
